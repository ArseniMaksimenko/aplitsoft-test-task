package com.example.user.draganddrop;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.content.ClipData;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;

public class DragActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag);




         final class MyTouchListener implements OnTouchListener {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    ClipData data = ClipData.newPlainText("", "");
                    DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                            view);
                    view.startDrag(data, shadowBuilder, view, 0);
                    view.setVisibility(View.VISIBLE);
                    return true;
                } else {
                    return false;
                }
            }
        }
        findViewById(R.id.button1).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.button2).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.button3).setOnTouchListener(new MyTouchListener());
        findViewById(R.id.button4).setOnTouchListener(new MyTouchListener());

        class MyDragListener implements OnDragListener {
            Drawable enterShape = getResources().getDrawable(
                    R.drawable.shape_droptarget);
            Drawable normalShape = getResources().getDrawable(R.drawable.shape);

            @Override
            public boolean onDrag(View v, DragEvent event) {
                int action = event.getAction();
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // do nothing
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        v.setBackgroundDrawable(enterShape);
                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        v.setBackgroundDrawable(normalShape);
                        break;
                    case DragEvent.ACTION_DROP:

                        View view = (View) event.getLocalState();
                        ViewGroup owner = (ViewGroup) view.getParent();
                        owner.removeView(view);
                        LinearLayout container = (LinearLayout) v;
                        container.addView(view);
                        view.setVisibility(View.VISIBLE);
                        break;
                    case DragEvent.ACTION_DRAG_ENDED:
                        v.setBackgroundDrawable(normalShape);
                    default:
                        break;
                }
                return true;
            }
        }
        findViewById(R.id.topl).setOnDragListener(new MyDragListener());
        findViewById(R.id.topr).setOnDragListener(new MyDragListener());
        findViewById(R.id.bottoml).setOnDragListener(new MyDragListener());
        findViewById(R.id.bottomr).setOnDragListener(new MyDragListener());
    }
}